package xivstats.web.controllers.twirl

import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import html.index
import xivstats.web.{AppController, Controller}

@Controller
class HiController extends AppController {

  override def route: Route = pathSingleSlash {
    get {
      htmlToResponseMarshalable(index())
    }
  }
}




