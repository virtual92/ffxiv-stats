package xivstats

import akka.actor.{ActorRef, ActorSystem, Props}
import com.typesafe.scalalogging.LazyLogging
import xivstats.web.WebServer

import scala.concurrent.ExecutionContextExecutor

object Main extends LazyLogging {

  val system = ActorSystem()
  private implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  val web: ActorRef = system.actorOf(Props[WebServer], "web")

  def main(args: Array[String]): Unit = {



  }


}
