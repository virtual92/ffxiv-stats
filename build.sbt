name := "ffxiv-stats"

version := "0.1"

scalaVersion := "2.12.3"

resolvers += "Adobe" at "https://repo.adobe.com/nexus/content/repositories/public/"
resolvers += Resolver.bintrayRepo("hseeberger", "maven")

libraryDependencies ++= Seq(
  "com.typesafe" % "config" % "1.3.1",
  "ch.qos.logback" % "logback-classic" % "1.2.3",
  "com.typesafe.akka" %% "akka-actor" % "2.5.4",
  "com.typesafe.akka" %% "akka-stream" % "2.5.4",
  "com.typesafe.akka" %% "akka-http" % "10.0.10",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2",
  "commons-io" % "commons-io" % "2.5",
  "org.apache.commons" % "commons-lang3" % "3.6",
  "com.github.hipjim" %% "scala-retry" % "0.2.2",
  "org.scala-lang.modules" %% "scala-java8-compat" % "0.8.0",
  "org.reflections" % "reflections" % "0.9.11",
  "com.typesafe.slick" %% "slick" % "3.2.1",
  "com.typesafe.slick" %% "slick-codegen" % "3.2.1",
  "com.chuusai" %% "shapeless" % "2.3.2"
)

javaOptions in Universal ++= Seq(
  "-J-Xmx512m",
  "-J-Xms512m"
)

mainClass in Compile := Some("xivstats.Main")

enablePlugins(SbtTwirl)

enablePlugins(JavaAppPackaging)
enablePlugins(DockerPlugin)
dockerRepository := Some("nowicki.azurecr.io")
dockerUsername := Some("nowicki")
dockerExposedPorts := Seq(8080)
dockerUpdateLatest := true

